          {-# LANGUAGE OverloadedStrings #-}
          import XMonad
          import XMonad.Config.Desktop
          import XMonad.Hooks.DynamicLog

          -- Prompt
          import XMonad.Prompt
          import XMonad.Prompt.RunOrRaise (runOrRaisePrompt)
          import XMonad.Prompt.AppendFile (appendFilePrompt)

          import System.Directory
          import System.IO (hPutStrLn)
          import System.Exit (exitSuccess)
          import qualified XMonad.StackSet as W

              -- Actions
          import XMonad.Actions.CopyWindow (kill1)
          import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
          import XMonad.Actions.GridSelect
          import XMonad.Actions.MouseResize
          import XMonad.Actions.Promote
          import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
          import XMonad.Actions.WindowGo (runOrRaise)
          import XMonad.Actions.WithAll (sinkAll, killAll)
          import qualified XMonad.Actions.Search as S

              -- Data
          import Data.Char (isSpace, toUpper)
          import Data.Monoid
          import Data.Maybe (fromJust, isJust, maybeToList)
          import Data.Tree
          import qualified Data.Map as M
          import Control.Monad ( join, when )

              -- Hooks
          import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
          import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
          import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, docks, ToggleStruts(..))
          import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
          import XMonad.Hooks.ServerMode
          import XMonad.Hooks.SetWMName
          import XMonad.Hooks.WorkspaceHistory
          import XMonad.Hooks.FadeInactive
          import XMonad.Hooks.UrgencyHook

              -- Layouts
          import XMonad.Layout.Accordion
          import XMonad.Layout.GridVariants (Grid(Grid))
          import XMonad.Layout.SimplestFloat
          import XMonad.Layout.Spiral
          import XMonad.Layout.ResizableTile
          import XMonad.Layout.Tabbed
          import XMonad.Layout.ThreeColumns

          import XMonad.Layout.Gaps
              ( Direction2D(D, L, R, U),
                gaps,
                setGaps,
                GapMessage(DecGap, ToggleGaps, IncGap) )

              -- Layouts modifiers
          import XMonad.Layout.LayoutModifier
          import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
          import XMonad.Layout.Magnifier
          import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
          import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
          import XMonad.Layout.NoBorders
          import XMonad.Layout.Renamed
          import XMonad.Layout.ShowWName
          import XMonad.Layout.Simplest
          import XMonad.Layout.Spacing
          import XMonad.Layout.SubLayouts
          import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
          import XMonad.Layout.WindowNavigation
          import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
          import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

             -- Utilities
          import XMonad.Util.Dmenu
          import XMonad.Util.EZConfig (additionalKeysP, additionalKeys)
          import XMonad.Util.NamedScratchpad
          import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
          import XMonad.Util.SpawnOnce

          import Color
          import Fonts
          import System.Taffybar.Support.PagerHints (pagerHints)

          myFont :: String
          myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

          myModMask :: KeyMask
          myModMask = mod4Mask        -- Sets modkey to super/windows key

          myTerminal :: String
          myTerminal = "${kitty}/bin/kitty"    -- Sets default terminal

          myBrowser :: String
          myBrowser = "google-chrome-unstable"  -- Sets qutebrowser as browser

          myEmacs :: String
          myEmacs = "${emacs}/bin/emacs"  -- Makes emacs keybindings easier to type

          myEditor :: String
          myEditor = "${emacs}/bin/emacs"  -- Sets emacs as editor

          -- Whether focus follows the mouse pointer.
          myFocusFollowsMouse :: Bool
          myFocusFollowsMouse = True

          -- Whether clicking on a window to focus also passes the click to the window
          myClickJustFocuses :: Bool
          myClickJustFocuses = True

          -- Width of the window border in pixels.
          myBorderWidth :: Dimension
          myBorderWidth = 2           -- Sets border width for windows

          myNormColor :: String       -- Border color of normal windows
          myNormColor   = colorBack   -- This variable is imported from Colors.THEME

          myFocusColor :: String      -- Border color of focused windows
          myFocusColor  = myFocusedBorderColor     -- This variable is imported from Colors.THEME

          windowCount :: X (Maybe String)
          windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

          -- Border colors for unfocused and focused windows, respectively.
          --
          myNormalBorderColor  = "#3b4252"
          myFocusedBorderColor = "#bc96da"

          addNETSupported :: Atom -> X ()
          addNETSupported x   = withDisplay $ \dpy -> do
              r               <- asks theRoot
              a_NET_SUPPORTED <- getAtom "_NET_SUPPORTED"
              a               <- getAtom "ATOM"
              liftIO $ do
                 sup <- (join . maybeToList) <$> getWindowProperty32 dpy a_NET_SUPPORTED r
                 when (fromIntegral x `notElem` sup) $
                   changeProperty32 dpy r a_NET_SUPPORTED a propModeAppend [fromIntegral x]

          addEWMHFullscreen :: X ()
          addEWMHFullscreen   = do
              wms <- getAtom "_NET_WM_STATE"
              wfs <- getAtom "_NET_WM_STATE_FULLSCREEN"
              mapM_ addNETSupported [wms, wfs]

          ------------------------------------------------------------------------
          -- Key bindings. Add, modify or remove key bindings here.
          --
          clipboardy :: MonadIO m => m () -- Don't question it
          clipboardy = spawn "rofi -modi \"\63053 :greenclip print\" -show \"\63053 \" -run-command '{cmd}' -theme ~/.config/rofi/launcher/style.rasi"

          centerlaunch = spawn "eww open-many blur_full weather profile quote search_full incognito-icon vpn-icon home_dir screenshot power_full reboot_full lock_full logout_full suspend_full"
          sidebarlaunch = spawn "eww open-many weather_side time_side smol_calendar player_side sys_side sliders_side"
          ewwclose = spawn "eww close-all"
          rofi_launcher = spawn "rofi -no-lazy-grab -show drun -modi run,drun,window -theme $HOME/.config/rofi/launcher/style -drun-icon-theme \"candy-icons\" "

          myStartupHook :: X ()
          myStartupHook = do
            -- spawnOnce "xrandr --output DP-1 --rotate left"
            spawnOnce "eww daemon"
            spawn "xsetroot -cursor_name left_ptr"
            spawnOnce "xset s 500"
            spawnOnce "xautolock -time 5 -locker \"${betterlockscreen}/bin/betterlockscreen --fx dim,pixel --lock dimblur\" -notify 30 -notifier \"${libnotify}/bin/notify-send 'Locker' 'Locking screen in 30 seconds'\" -killtime 5 -killer \"systemctl suspend\""
            spawnOnce "${picom}/bin/picom --experimental-backends"
            spawnOnce "${haskellPackages.greenclip}/bin/greenclip daemon"
            -- spawnOnce "dunst"
            spawnOnce "${feh}/bin/feh --bg-scale ${self}/awesome.dratrion/wallpaper/wallpaper.jpg"
            spawn "${conky}/bin/conky -c ~/.conky/conky_system -y100 -c ~/conky_and_lua_by_mr_mattz_danuesx/.conkyrc"
            return ()

          myColorizer :: Window -> Bool -> X (String, String)
          myColorizer = colorRangeFromClassName
                            (0x28,0x2c,0x34) -- lowest inactive bg
                            (0x28,0x2c,0x34) -- highest inactive bg
                            (0xc7,0x92,0xea) -- active bg
                            (0xc0,0xa7,0x9a) -- inactive fg
                            (0x28,0x2c,0x34) -- active fg

          -- gridSelect menu layout
          mygridConfig :: p -> GSConfig Window
          mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
              { gs_cellheight   = 40
              , gs_cellwidth    = 200
              , gs_cellpadding  = 6
              , gs_originFractX = 0.5
              , gs_originFractY = 0.5
              , gs_font         = myFont
              }

          spawnSelected' :: [(String, String)] -> X ()
          spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
              where conf = def
                             { gs_cellheight   = 40
                             , gs_cellwidth    = 200
                             , gs_cellpadding  = 6
                             , gs_originFractX = 0.5
                             , gs_originFractY = 0.5
                             , gs_font         = myFont
                             }

          --Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
          mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
          mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

          -- Below is a variation of the above except no borders are applied
          -- if fewer than two windows. So a single window has no gaps.
          mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
          mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

          -- Defining a bunch of layouts, many that I don't use.
          -- limitWindows n sets maximum number of windows displayed for layout.
          -- mySpacing n sets the gap size around the windows.
          tall     = renamed [Replace "tall"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ limitWindows 12
                     $ mySpacing 8
                     $ ResizableTall 1 (3/100) (1/2) []
          magnify  = renamed [Replace "magnify"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ magnifier
                     $ limitWindows 12
                     $ mySpacing 8
                     $ ResizableTall 1 (3/100) (1/2) []
          monocle  = renamed [Replace "monocle"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ limitWindows 20 Full
          floats   = renamed [Replace "floats"]
                     $ smartBorders
                     $ limitWindows 20 simplestFloat
          grid     = renamed [Replace "grid"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ limitWindows 12
                     $ mySpacing 8
                     $ mkToggle (single MIRROR)
                     $ Grid (16/10)
          spirals  = renamed [Replace "spirals"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ mySpacing' 8
                     $ spiral (6/7)
          threeCol = renamed [Replace "threeCol"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ limitWindows 7
                     $ ThreeCol 1 (3/100) (1/2)
          threeRow = renamed [Replace "threeRow"]
                     $ smartBorders
                     $ windowNavigation
                     $ addTabs shrinkText myTabTheme
                     $ subLayout [] (smartBorders Simplest)
                     $ limitWindows 7
                     -- Mirror takes a layout and rotates it by 90 degrees.
                     -- So we are applying Mirror to the ThreeCol layout.
                     $ Mirror
                     $ ThreeCol 1 (3/100) (1/2)
          tabs     = renamed [Replace "tabs"]
                     -- I cannot add spacing to this layout because it will
                     -- add spacing between window and tabs which looks bad.
                     $ tabbed shrinkText myTabTheme
          tallAccordion  = renamed [Replace "tallAccordion"]
                     $ Accordion
          wideAccordion  = renamed [Replace "wideAccordion"]
                     $ Mirror Accordion

          -- setting colors for tabs layout and tabs sublayout.
          myTabTheme = def { activeColor         = color15
                           , inactiveColor       = color08
                           , activeBorderColor   = color15
                           , inactiveBorderColor = colorBack
                           , activeTextColor     = colorBack
                           , inactiveTextColor   = color16
                           }

          -- Theme for showWName which prints current workspace when you change workspaces.
          myShowWNameTheme :: SWNConfig
          myShowWNameTheme = def
              { swn_fade              = 1.0
              , swn_bgcolor           = "#1c1f24"
              , swn_color             = "#ffffff"
              }

          -- The layout hook
          myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
                         $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
                       where
                         myDefaultLayout =     withBorder myBorderWidth tall
                                           ||| magnify
                                           ||| noBorders monocle
                                           ||| floats
                                           ||| noBorders tabs
                                           ||| grid
                                           ||| spirals
                                           ||| threeCol
                                           ||| threeRow
                                           ||| tallAccordion
                                           ||| wideAccordion

          myWorkspaces = zipWith (\i w -> show i <> ":" <> w) [1 ..] ["xterm", "www", "remote", "dev", "chat", "doc", "vid"]
          myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..]

          clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
              where i = fromJust $ M.lookup ws myWorkspaceIndices

          myManageHook :: ManageHook
          myManageHook = composeAll . concat $
            [ [ resource =? r --> doIgnore | r <- myIgnores ]
            , [ className =? c --> doShift "1:xterm" | c <- myXterm ]
            , [ className =? c --> doShift "2:www" | c <- myWww ]
            , [ className =? c --> doShift "3:remote" | c <- myRemote ]
            , [ className =? c --> doShift "4:dev" | c <- myDev ]
            , [ className =? c --> doShift "5:chat" | c <- myChat ]
            , [ className =? c --> doShift "6:doc" | c <- myDoc ]
            , [ className =? c --> doShift "7:vid" | c <- myVid ]
            , [ className =? c --> doCenterFloat | c <- myFloats ]
            , [ name =? n --> doCenterFloat | n <- myNames ]
            , [ isFullscreen --> myDoFullFloat ]
            ]
            where
              myIgnores = []
              myXterm = ["kitty"]
              myWww = ["Firefox", "firefox", "Navigator", "Google-chrome-unstable", "google-chrome-unstable"]
              myRemote = ["Wfica"]
              myDev = ["Eclipse", "java", "Java", "Emacs", "emacs", "Gimp", "gimp", "VirtualBox Manager", "Inkscape", "org.inkscape.Inkscape"]
              myChat = ["discord"]
              myDoc = ["Evince", "evince", "libreoffice", "libreoffice-startcenter"]
              myVid = ["vlc", "obs", "zoom"]
              myNames = ["Google Chrome"]
              myFloats = ["Firefox", "firefox", "Navigator", "Gimp", "obs", "feh"]

              name = stringProperty "WM_NAME"
              role = stringProperty "WM_WINDOW_ROLE"

              myDoFullFloat :: ManageHook
              myDoFullFloat = doF W.focusDown <+> doFullFloat

          -- Prompt Config {{{
          mXPConfig :: XPConfig
          mXPConfig =
              defaultXPConfig { font                  = barFont
                              , bgColor               = colorDarkGray
                              , fgColor               = colorGreen
                              , bgHLight              = colorGreen
                              , fgHLight              = colorDarkGray
                              , promptBorderWidth     = 0
                              , height                = 14
                              , historyFilter         = deleteConsecutive
                              }

          -- Run or Raise Menu
          largeXPConfig :: XPConfig
          largeXPConfig = mXPConfig
                          { font = xftFont
                          , height = 22
                          }
          -- }}}
          ------------------------------------------------------------------------
          -- Mouse bindings: default actions bound to mouse events
          --
          myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

              -- mod-button1, Set the window to floating mode and move by dragging
              [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                                 >> windows W.shiftMaster))

              -- mod-button2, Raise the window to the top of the stack
              , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

              -- mod-button3, Set the window to floating mode and resize by dragging
              , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                                 >> windows W.shiftMaster))

              -- you may also bind events to the mouse scroll wheel (button4 and button5)
              ]

          -- START_KEYS
          myKeys conf@(XConfig {XMonad.modMask = modMask}) = conf `additionalKeys`
            [ ((modMask, xK_r), spawn "dmenu_run -i -p \"Run: \"")
            , ((modMask, xK_l), spawn "betterlockscreen --fx dim,pixel --lock dimblur")
            -- terminals
            , ((modMask .|. shiftMask, xK_Return), windows W.swapMaster)
            , ((modMask, xK_Return), spawn $ XMonad.terminal conf)
            , ((modMask, xK_x), spawn "${xterm}/bin/xterm")
             -- multimedia keys
            , ((0, 0x1008ff12), spawn "${pulsemixer}/bin/pulsemixer --toggle-mute")
            , ((0, 0x1008ff11), spawn "${pulsemixer}/bin/pulsemixer --change-volume -10")
            , ((0, 0x1008ff13), spawn "${pulsemixer}/bin/pulsemixer --change-volume +10")
            -- sizing windows
            , ((modMask .|. shiftMask, xK_h), sendMessage XMonad.Shrink)
            , ((modMask .|. shiftMask, xK_l), sendMessage XMonad.Expand)
            -- launch rofi and dashboard
            , ((modMask,               xK_o     ), rofi_launcher)
            , ((modMask,               xK_p     ), centerlaunch)
            , ((modMask .|. shiftMask, xK_p     ), ewwclose)
            -- xrandr rotate
            , ((modMask .|. controlMask, xK_l), spawn "xrandr --output DP-1 --rotate left")
            , ((modMask .|. controlMask, xK_r), spawn "xrandr --output DP-1 --rotate right")
            , ((modMask .|. controlMask, xK_n), spawn "xrandr --output DP-1 --rotate normal")
            -- launch eww sidebar
            , ((modMask,               xK_s     ), sidebarlaunch)
            , ((modMask .|. shiftMask, xK_s     ), ewwclose)
            -- GAPS!!!
            , ((modMask .|. controlMask, xK_g), sendMessage $ ToggleGaps)               -- toggle all gaps
            , ((modMask .|. shiftMask, xK_g), sendMessage $ setGaps [(L,30), (R,30), (U,40), (D,60)]) -- reset the GapSpec

            , ((modMask .|. controlMask, xK_t), sendMessage $ IncGap 10 L)              -- increment the left-hand gap
            , ((modMask .|. shiftMask, xK_t     ), sendMessage $ DecGap 10 L)           -- decrement the left-hand gap

            , ((modMask .|. controlMask, xK_y), sendMessage $ IncGap 10 U)              -- increment the top gap
            , ((modMask .|. shiftMask, xK_y     ), sendMessage $ DecGap 10 U)           -- decrement the top gap

            , ((modMask .|. controlMask, xK_u), sendMessage $ IncGap 10 D)              -- increment the bottom gap
            , ((modMask .|. shiftMask, xK_u     ), sendMessage $ DecGap 10 D)           -- decrement the bottom gap

            , ((modMask .|. controlMask, xK_i), sendMessage $ IncGap 10 R)              -- increment the right-hand gap
            , ((modMask .|. shiftMask, xK_i     ), sendMessage $ DecGap 10 R)           -- decrement the right-hand gap
            ]
          myKeys_ :: [(String, X ())]
          myKeys_ =
              -- KB_GROUP Xmonad
                  [ ("M-C-r", spawn "xmonad --recompile")       -- Recompiles xmonad
                  , ("M-S-r", spawn "xmonad --restart")         -- Restarts xmonad
                  , ("M-S-q", io exitSuccess)                   -- Quits xmonad

              -- KB_GROUP Get Help
                  , ("M-S-/", spawn "~/.xmonad/xmonad_keys.sh") -- Get list of keybindings
                  , ("M-/", spawn "dtos-help")                  -- DTOS help/tutorial videos

              -- KB_GROUP Run Prompt
                  , ("M-S-<Return>", spawn "dmenu_run -i -p \"Run: \"") -- Dmenu

              -- KB_GROUP Other Dmenu Prompts
              -- In Xmonad and many tiling window managers, M-p is the default keybinding to
              -- launch dmenu_run, so I've decided to use M-p plus KEY for these dmenu scripts.
                  , ("M-p h", spawn "dm-hub")           -- allows access to all dmscripts
                  , ("M-p a", spawn "dm-sounds")        -- choose an ambient background
                  , ("M-p b", spawn "dm-setbg")         -- set a background
                  , ("M-p c", spawn "dtos-colorscheme") -- choose a colorscheme
                  , ("M-p C", spawn "dm-colpick")       -- pick color from our scheme
                  , ("M-p e", spawn "dm-confedit")      -- edit config files
                  , ("M-p i", spawn "dm-maim")          -- screenshots (images)
                  , ("M-p k", spawn "dm-kill")          -- kill processes
                  , ("M-p m", spawn "dm-man")           -- manpages
                  , ("M-p n", spawn "dm-note")          -- store one-line notes and copy them
                  , ("M-p o", spawn "dm-bookman")       -- qutebrowser bookmarks/history
                  , ("M-p p", spawn "passmenu")         -- passmenu
                  , ("M-p q", spawn "dm-logout")        -- logout menu
                  , ("M-p r", spawn "dm-reddit")        -- reddio (a reddit viewer)
                  , ("M-p s", spawn "dm-websearch")     -- search various search engines
                  , ("M-p t", spawn "dm-translate")     -- translate text (Google Translate)

              -- KB_GROUP Useful programs to have a keybinding for launch
                  , ("M-<Return>", spawn (myTerminal))
                  , ("M-b", spawn (myBrowser))
                  , ("M-M1-h", spawn (myTerminal ++ " -e htop"))

              -- KB_GROUP Kill windows
                  , ("M-S-c", kill1)     -- Kill the currently focused client
                  , ("M-S-a", killAll)   -- Kill all windows on current workspace

              -- KB_GROUP Workspaces
                  , ("M-.", nextScreen)  -- Switch focus to next monitor
                  , ("M-,", prevScreen)  -- Switch focus to prev monitor
                  , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
                  , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws

              -- KB_GROUP Floating windows
                  , ("M-f", sendMessage (T.Toggle "floats")) -- Toggles my 'floats' layout
                  , ("M-t", withFocused $ windows . W.sink)  -- Push floating window back to tile
                  , ("M-S-t", sinkAll)                       -- Push ALL floating windows to tile

              -- KB_GROUP Increase/decrease spacing (gaps)
                  , ("C-M1-j", decWindowSpacing 4)         -- Decrease window spacing
                  , ("C-M1-k", incWindowSpacing 4)         -- Increase window spacing
                  , ("C-M1-h", decScreenSpacing 4)         -- Decrease screen spacing
                  , ("C-M1-l", incScreenSpacing 4)         -- Increase screen spacing

              -- KB_GROUP Grid Select (CTR-g followed by a key)
              --    , ("C-g g", spawnSelected' myAppGrid)                 -- grid select favorite apps
                  , ("C-g t", goToSelected $ mygridConfig myColorizer)  -- goto selected window
                  , ("C-g b", bringSelected $ mygridConfig myColorizer) -- bring selected window

              -- KB_GROUP Windows navigation
                  , ("M-m", windows W.focusMaster)  -- Move focus to the master window
                  , ("M-j", windows W.focusDown)    -- Move focus to the next window
                  , ("M-k", windows W.focusUp)      -- Move focus to the prev window
                  , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
                  , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
                  , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
                  , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
                  , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
                  , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

              -- KB_GROUP Layouts
                  , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
                  , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full

              -- KB_GROUP Increase/decrease windows in the master pane or the stack
                  , ("M-S-<Up>", sendMessage (IncMasterN 1))      -- Increase # of clients master pane
                  , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease # of clients master pane
                  , ("M-C-<Up>", increaseLimit)                   -- Increase # of windows
                  , ("M-C-<Down>", decreaseLimit)                 -- Decrease # of windows

              -- KB_GROUP Window resizing
                  , ("M-h", sendMessage Shrink)                   -- Shrink horiz window width
                  , ("M-l", sendMessage Expand)                   -- Expand horiz window width
                  , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
                  , ("M-M1-k", sendMessage MirrorExpand)          -- Expand vert window width

              -- KB_GROUP Sublayouts
              -- This is used to push windows to tabbed sublayouts, or pull them out of it.
                  , ("M-C-h", sendMessage $ pullGroup L)
                  , ("M-C-l", sendMessage $ pullGroup R)
                  , ("M-C-k", sendMessage $ pullGroup U)
                  , ("M-C-j", sendMessage $ pullGroup D)
                  , ("M-C-m", withFocused (sendMessage . MergeAll))
                  -- , ("M-C-u", withFocused (sendMessage . UnMerge))
                  , ("M-C-/", withFocused (sendMessage . UnMergeAll))
                  , ("M-C-.", onGroup W.focusUp')    -- Switch focus to next tab
                  , ("M-C-,", onGroup W.focusDown')  -- Switch focus to prev tab

              -- KB_GROUP Scratchpads
              -- Toggle show/hide these programs.  They run on a hidden workspace.
              -- When you toggle them to show, it brings them to your current workspace.
              -- Toggle them to hide and it sends them back to hidden workspace (NSP).
              --    , ("M-s t", namedScratchpadAction myScratchPads "terminal")
              --    , ("M-s m", namedScratchpadAction myScratchPads "mocp")
              --    , ("M-s c", namedScratchpadAction myScratchPads "calculator")

              -- KB_GROUP Controls for mocp music player (SUPER-u followed by a key)
                  , ("M-u p", spawn "mocp --play")
                  , ("M-u l", spawn "mocp --next")
                  , ("M-u h", spawn "mocp --previous")
                  , ("M-u <Space>", spawn "mocp --toggle-pause")

              -- KB_GROUP Emacs (SUPER-e followed by a key)
                  , ("M-e e", spawn (myEmacs ++ ("--eval '(dashboard-refresh-buffer)'")))   -- emacs dashboard
                  , ("M-e b", spawn (myEmacs ++ ("--eval '(ibuffer)'")))   -- list buffers
                  , ("M-e d", spawn (myEmacs ++ ("--eval '(dired nil)'"))) -- dired
                  , ("M-e i", spawn (myEmacs ++ ("--eval '(erc)'")))       -- erc irc client
                  , ("M-e n", spawn (myEmacs ++ ("--eval '(elfeed)'")))    -- elfeed rss
                  , ("M-e s", spawn (myEmacs ++ ("--eval '(eshell)'")))    -- eshell
                  , ("M-e t", spawn (myEmacs ++ ("--eval '(mastodon)'")))  -- mastodon.el
                  , ("M-e v", spawn (myEmacs ++ ("--eval '(+vterm/here nil)'"))) -- vterm if on Doom Emacs
                  , ("M-e w", spawn (myEmacs ++ ("--eval '(doom/window-maximize-buffer(eww \"distro.tube\"))'"))) -- eww browser if on Doom Emacs
                  , ("M-e a", spawn (myEmacs ++ ("--eval '(emms)' --eval '(emms-play-directory-tree \"~/Music/\")'")))

              -- KB_GROUP Multimedia Keys
                  , ("<XF86AudioPlay>", spawn "mocp --play")
                  , ("<XF86AudioPrev>", spawn "mocp --previous")
                  , ("<XF86AudioNext>", spawn "mocp --next")
                  , ("<XF86AudioMute>", spawn "amixer set Master toggle")
                  , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
                  , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
                  , ("<XF86HomePage>", spawn "qutebrowser https://www.youtube.com/c/DistroTube")
                  , ("<XF86Search>", spawn "dm-websearch")
                  , ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
                  , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
                  , ("<XF86Eject>", spawn "toggleeject")
                  , ("<Print>", spawn "dm-maim")
                  ]
              -- The following lines are needed for named scratchpads.
                    where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                          nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
          -- END_KEYS
          -- myLogHook :: Handle -> X ()
          myLogHook h = dynamicLogWithPP $ defaultPP
              {
                  ppCurrent           =   dzenColor "#ebac54" "#1B1D1E" . pad
                , ppVisible           =   dzenColor "white" "#1B1D1E" . pad
                , ppHidden            =   dzenColor "white" "#1B1D1E" . pad
                , ppHiddenNoWindows   =   dzenColor "#7b7b7b" "#1B1D1E" . pad
                , ppUrgent            =   dzenColor "#ff0000" "#1B1D1E" . pad
                , ppWsSep             =   " "
                , ppSep               =   "  |  "
                , ppTitle             =   (" " ++) . dzenColor "white" "#1B1D1E" . dzenEscape
                , ppOutput            =   hPutStrLn h
              }
          main = do
            xmproc <- spawnPipe "${xmobar}/bin/xmobar"
            let cfg = desktopConfig {
                terminal = myTerminal
              , modMask  = myModMask
              , borderWidth = myBorderWidth
              , focusFollowsMouse  = myFocusFollowsMouse
              , clickJustFocuses   = myClickJustFocuses
              , manageHook = myManageHook <+> manageDocks <+> manageHook desktopConfig
              , layoutHook = gaps [(L,1), (R,240), (U,0), (D,0)] $ spacingRaw True (Border 5 5 5 5) True (Border 5 5 5 5) True $ smartBorders $ avoidStruts (myLayoutHook)
              -- , logHook = myLogHook dzenLeftBar >> fadeInactiveLogHook 0xdddddddd
              , logHook = dynamicLogWithPP xmobarPP
                  { ppOutput = hPutStrLn xmproc
                  , ppTitle = xmobarColor "green" "" . shorten 50
                  }
              , handleEventHook = docksEventHook
              , workspaces = myWorkspaces
              , mouseBindings      = myMouseBindings
              , focusedBorderColor = myFocusedBorderColor
              , normalBorderColor = myNormalBorderColor
              , startupHook        = myStartupHook >> addEWMHFullscreen
              }
            xmonad $ docks $ ewmh $ pagerHints $ myKeys $ cfg
