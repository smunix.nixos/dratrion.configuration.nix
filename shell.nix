{ pkgs ? import <nixpkgs> { } }:

with pkgs;
let
  nixBin = writeShellScriptBin "nix" ''
    ${nixGit}/bin/nix --option experimental-features "nix-command flakes" "$@"
  '';

in mkShell {
  buildInputs = [ git nix-bash-completions ];

  shellHook = ''
    export FLAKE="$(pwd)"
    export PATH="$FLAKE/bin:${nixBin}/bin:$PATH"
  '';
}
