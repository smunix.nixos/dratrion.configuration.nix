{
  description = "λ well-tailored and configureable NixOS system!";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=master";
    nixpkgs-unstable.url = "github:nixos/nixpkgs?ref=master";

    home-manager.url = "github:nix-community/home-manager/master";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";

    ## (remove) xmonad & contrib after "ConditionalLayoutModifier" merger
    xmonad.url = "github:xmonad/xmonad";
    xmonad-contrib.url = "github:icy-thought/xmonad-contrib";
    taffybar.url = "github:smunix/taffybar?ref=fix.taffy";

    emacs.url = "github:nix-community/emacs-overlay";
    neovim-nightly.url = "github:nix-community/neovim-nightly-overlay";
    rust.url = "github:oxalica/rust-overlay";

    zig.url = "github:arqv/zig-overlay";

    hls.url = "github:haskell/haskell-language-server?ref=master";

    nix.url = "github:nixos/nix?rev=4bb111c8d4c5692db2f735c2803f632f8c30b6ab";
    # nix.url = "github:nixos/nix?ref=master";
  };

  outputs = inputs@{ self, nixpkgs, nixpkgs-unstable, ... }:

    let
      inherit (lib.my) mapModules mapModulesRec mapHosts;

      system = "x86_64-linux";

      mkPkgs = pkgs: extraOverlays:
        import pkgs {
          inherit system;
          config.allowUnfree = true;
          overlays = extraOverlays ++ [ inputs.nix.overlay inputs.hls.overlays.default ]
            ++ (lib.attrValues self.overlays);
        };

      pkgs = mkPkgs nixpkgs [ self.overlay ];
      pkgs' = mkPkgs nixpkgs-unstable [ ];

      lib = nixpkgs.lib.extend (self: super: {
        my = import ./lib {
          inherit pkgs inputs;
          lib = self;
        };
      });

    in {
      lib = lib.my;

      overlay = final: prev: {
        unstable = pkgs';
        my = self.packages."${system}";
        nixGit = final.nix;
      };

      overlays = mapModules ./overlays import;

      packages."${system}" = mapModules ./packages (p: pkgs.callPackage p { });

      nixosModules = {
        snowflake = import ./.;
      } // mapModulesRec ./modules import;

      nixosConfigurations = mapHosts ./hosts { };

      devShell."${system}" = import ./shell.nix { inherit pkgs; };

      # TODO: new struct.
      templates.full = {
        path = ./.;
        description = "λ well-tailored and configureable NixOS system!";
      };

      template.minimal = {
        path = ./templates/minimal;
        description = "λ well-tailored and configureable NixOS system!";
      };

      defaultTemplate = self.templates.minimal;

      # TODO: deployment + template tool.
      # defaultApp."${system}" = {
      #   type = "app";
      #   program = ./bin/hagel;
      # };

    };
}
