{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.v4l2;
in {
  options.modules.hardware.v4l2 = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    boot.extraModulePackages = with unstable; [
      config.boot.kernelPackages.v4l2loopback.out
    ];
    boot.kernelModules = [
      "v4l2loopback"
    ];
    boot.extraModprobeConfig = ''
      options v4l2loopback exclusive_caps=1 video_nr=1 card_label=v4l2_1
    '';
  };
}
