{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.appliances.browsers.chrome;
in {
  options.modules.desktop.appliances.browsers.chrome = with types; {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable (mkMerge [{
    nixpkgs.config.allowUnfree = true;
    user.packages = with pkgs; [ google-chrome-dev ];
  }]);
}
