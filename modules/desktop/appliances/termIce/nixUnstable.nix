{ config, options, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.desktop.appliances.termIce.nixUnstable;
in {
  options.modules.desktop.appliances.termIce.nixUnstable = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [ nix ];
  };
}
