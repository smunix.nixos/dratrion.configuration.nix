{ config, options, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.appliances.editors;
in {
  options.modules.desktop.appliances.editors = {
    default = mkOpt types.str "emacs";
  };

  config = (mkMerge [
    (mkIf (cfg.default != null) { env.EDITOR = cfg.default; })

    (mkIf (cfg.emacs.enable || cfg.nvim.enable) {

      nixpkgs.overlays = [ inputs.hls.overlays.default ];

      user.packages = with pkgs; [
        #extraPkgs
        dtrx
        fd
        imagemagick
        (ripgrep.override { withPCRE2 = true; })
        # toolbox
        editorconfig-core-c
        # module dependencies
        ## checkers: aspell
        (aspellWithDicts (ds: with ds; [ en en-computers en-science ]))
        ## lsp: haskell
        # inputs.hls.packages.x86_64-linux.haskell-language-server-921
        stylish-haskell
        ## lsp: LaTeX
        tectonic
        gnuplot
        ## lsp: Org-Mode
        pandoc
        ## lsp: javascript
        nodePackages.typescript-language-server
        ## lsp: nix
        nixfmt
        rnix-lsp
        ## lsp: rust
        unstable.rust-analyzer
        silver-searcher
      ];
    })
  ]);
}
