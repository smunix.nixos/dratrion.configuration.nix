{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.shell;
  configDir = config.snowflake.configDir;
in {
  options.modules.shell.fish = { enable = mkBoolOpt false; };

  config = mkIf cfg.fish.enable {
    users.defaultUserShell = pkgs.fish;

    user.packages = with pkgs; [
      any-nix-shell
      bat
      bottom
      exa
      fd
      file
      fzf
      hyperfine
      lorri
      nixfmt
      nix-top
      pipes-rs
      pwgen
      ripgrep
      tldr
      tree
      xdot
      yt-dlp
    ];

    # Enables vendor completion: https://nixos.wiki/wiki/Fish
    programs.fish.enable = true;

    homeManager.programs = {
      lsd.enable = true;
      fzf.enable = true;
    };

    homeManager.programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
      config.whitelist.prefix = [ "/home" ];
    };

    homeManager.programs.fish = {
      enable = true;
      interactiveShellInit = ''
        if not set -q TMUX
          exec tmux
        end

        ${pkgs.starship}/bin/starship init fish | source
        ${pkgs.any-nix-shell}/bin/any-nix-shell fish | source
        ${builtins.readFile "${configDir}/fish/interactive.fish"}

        ${optionalString cfg.git.enable ''
          fish -c "__git.init" || true
        ''}
      '';

      functions = {
        eg = "emacs --create-frame $argv & disown";
        ecg = "emacsclient --create-frame $argv & disown";
        gitignore = "curl -sL https://www.gitignore.io/api/$argv";
      };

      shellAbbrs = {
        ls = "lsd";
        l = "lsd -al";
        ll = "lsd -l";
        clip = "xclip -sel clip";
        # emacs
        e = "emacs -nw";
        eclient = "emacsclient -c";
        # git
        ga = "git add";
        gc = "git commit -v";
        "gc!" = "git commit -v --amend";
        gl = "git pull";
        gf = "git fetch";
        gco = "git checkout";
        gd = "git diff";
        gsh = "git show";
        gst = "git status";
        gb = "git branch";
        gsta = "git stash";
        gstp = "git stash pop";
        glg = "git log --stat";
        glga = "git log --stat --graph --all";
        glo = "git log --oneline";
        gloa = "git log --oneline --graph --all";
        grh = "git reset HEAD";
        # nix
        n = "nix";
      };

      shellAliases = {
        lt = "lsd --tree -I node_modules -I build -I target -I __pycache__";
      };

      plugins = [
        {
          name = "done";
          src = pkgs.fetchFromGitHub {
            owner = "franciscolourenco";
            repo = "done";
            rev = "d6abb267bb3fb7e987a9352bc43dcdb67bac9f06";
            sha256 = "1h8v5jg9kkali50qq0jn0i1w68wp4c2l0fapnglnnpg0v4vv51za";
          };
        }
        {
          name = "fzf";
          src = pkgs.fetchFromGitHub {
            owner = "PatrickF1";
            repo = "fzf.fish";
            rev = "628f04bf239bc6b820c090b8c814ca3c242142d7";
            sha256 = "EiNaEhqJbisv/gA5th2IWh4XIQ3zlGrXTpMMTaUb0Ag=";
          };
        }
        {
          name = "fish-abbreviation-tips";
          src = pkgs.fetchFromGitHub {
            owner = "Gazorby";
            repo = "fish-abbreviation-tips";
            rev = "d29a52375a0826ed86b0710f58b2495a73d3aff3";
            sha256 = "0s6zcxlhfys545lnfg626ilk1jqgak9xpijy3jxs9z12w2c4d3gk";
          };
        }
        (mkIf cfg.git.enable {
          name = "git";
          src = pkgs.fetchFromGitHub {
            owner = "jhillyerd";
            repo = "plugin-git";
            rev = "1a0357c1f13a9c5f18a5d773e9c0c963f1ff23b6";
            sha256 = "d1Mcn+u9aLI3PLDXth0VyoqZiO6Z6R/yL7f/RwIR0/o=";
          };
        })
      ];
    };
  };
}
