{ config, options, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let
  devCfg = config.modules.develop;
  cfg = devCfg.haskell;
in {
  options.modules.develop.haskell = {
    enable = mkBoolOpt false;
    ghc = mkOption {
      type = types.str;
      default = "ghc_9_2_2";
      example = "ghc_9_2_2";
      description = "GHC version";
    };
    xdg.enable = mkBoolOpt devCfg.xdg.enable;
  };

  config = mkMerge [
    (mkIf cfg.enable {
      user.packages = let hpkgs = pkgs.haskellPackages."${cfg.ghc}";
      in with pkgs;
      with haskellPackages;
      with hpkgs; [
        ghc
        cabal-install
        hpack
        haskell-language-server
        graphmod
        hasktags
        hoogle
        implicit-hie
      ];
    })

    (mkIf cfg.xdg.enable {
      # TODO
    })
  ];
}
